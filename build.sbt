name := "AdventOfCode2019"

version := "0.1"

lazy val common = Seq(
  scalaVersion := "2.13.1",
  libraryDependencies ++= Seq(
    "com.chuusai" %% "shapeless" % "2.3.3",
    "org.typelevel" %% "cats-core" % "2.0.0",
    "org.typelevel" %% "cats-effect" % "2.0.0",
    "org.scala-lang.modules" %% "scala-collection-contrib" % "0.1.0"
  )
)

lazy val day1 = project in file("day1") settings common
lazy val day2 = project in file("day2") settings common
lazy val day5 = project in file("day5") settings common
lazy val day6 = project in file("day6") settings common
lazy val day7 = project in file("day7") settings common dependsOn day5
lazy val day8 = project in file("day8") settings common
lazy val day9 = project in file("day9") settings common dependsOn day5
lazy val day11 = project in file("day11") settings common dependsOn day5
lazy val day12 = project in file("day12") settings common
lazy val day13 = project in file("day13") settings common dependsOn day5
lazy val day14 = project in file("day14") settings common
lazy val root = project in file(".") aggregate(day1, day2, day5, day6, day7, day8, day9, day11, day12, day13, day14)
