import scala.io.Source.fromResource
import LazyList.iterate
import scala.language.postfixOps

object Day1 extends App {
  private def readMasses: LazyList[Int] =
    fromResource("Day1.txt")
      .getLines
      .map{_.toInt}
      .to(LazyList)

  private def calculateFuel(mass: Int): Int = mass / 3 - 2

  def part1: LazyList[Int] = readMasses map calculateFuel

  def part2: LazyList[Int] = part1 flatMap {iterate(_)(calculateFuel) takeWhile(_ > 0)}

  println(s"Part1: ${part1 sum}")
  println(s"Part2: ${part2 sum}")
}
