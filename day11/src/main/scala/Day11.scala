import Day5._

import math.floorMod
import Map.empty
import LazyList.unfold
import cats.implicits._

object Day11 extends App {
  type Point = (Int, Int)
  type Color = Long
  type ColorsMap = Map[Point, Color]

  val UP    = (0, -1)
  val DOWN  = (0,  1)
  val LEFT  = (-1, 0)
  val RIGHT = (1 , 0)

  val DIRECTIONS_MAPPING = Map(0L -> UP, 1L -> RIGHT, 2L -> DOWN, 3L -> LEFT)

  private val registries = readInstructions("Day11.txt")

  case class PaintedMap(position: Point = (0, 0), direction: Long = 0, fields: ColorsMap = empty.withDefaultValue(0L))

  def part1(map: PaintedMap = PaintedMap()): LazyList[ColorsMap] =
    unfold{State(registries) -> map}{case (state, PaintedMap(prevPosition, prevDirection, fields)) =>
      compute{state.enqueue(fields(prevPosition))} take 2 match {
        case LazyList((_, color), (nextState, rotation)) =>
          val direction     = floorMod(prevDirection + {if(rotation == 0) -1 else 1}, 4L)
          val position      = prevPosition |+| DIRECTIONS_MAPPING(direction)
          val updatedFields = fields.updated(prevPosition, color)

          Some{updatedFields -> (nextState, PaintedMap(position, direction, updatedFields))}

        case _ => None
      }
    }

  def part2: Unit = {
    val colored = part1(PaintedMap(fields = PaintedMap().fields.updated((0, 0), 1))).last
    val minX = colored.map{case ((x, _), _) => x}.min
    val maxX = colored.map{case ((x, _), _) => x}.max
    val minY = colored.map{case ((_, y), _) => y}.min
    val maxY = colored.map{case ((_, y), _) => y}.max

    for {
      y <- minY to maxY
      x <- minX to maxX
    } {
      if (x == 0) println()

      print(if(colored(x -> y) == 0L) '.' else '#')
    }
  }

  println(s"part1: ${part1().last.size}")
  println(s"part2:")
  part2
}
