import scala.io.Source.fromResource
import cats.implicits.{catsKernelStdCommutativeGroupForTuple3, catsSyntaxSemigroup, catsKernelStdGroupForInt}
import shapeless._
import syntax.std.tuple._
import scala.language.postfixOps

import math.{signum, abs}
import Seq.fill
import Set.empty

object Day12 extends App {
  type Point = (Int, Int, Int)
  val ZERO = (0, 0, 0)

  private val regex = """<x=(-?\d+)\s*,\s*y=(-?\d+)\s*,\s*z=(-?\d+)\s*>.*""".r

  private def readPlanets: Seq[Point] =
    fromResource("Day12.txt")
      .getLines
      .map{case regex(x, y, z) => (x.toInt, y.toInt, z.toInt)}
      .toSeq

  private object Compare extends Poly1 {
    implicit val compare: Case.Aux[(Int, Int), Int] = at{case (x:Int, y:Int) => signum(y - x)}
  }

  def calculateVelocity(axis: Seq[Point]): Option[Seq[Point]] = {
    val accumulated = for {
      (others, current) <- axis.tails.drop(1).zip(axis)
      otherCompares     = others.map(other => (other, current).zip.map(Compare))
      currentCompares   = others.map(other => (current, other).zip.map(Compare)).foldLeft(ZERO)(_ |+| _)
      allCompares       = currentCompares +: otherCompares
      paddedCompares    = fill(axis.size - others.size - 1){ZERO} ++ allCompares
    } yield paddedCompares

    accumulated.reduceOption(_.zip(_).map{case (l, r) => l |+| r})
  }

  private def calculatePositionAndVelocity(points: Seq[Point]): LazyList[(Seq[Point], Seq[Point])] = {
    lazy val positionAndVelocity: LazyList[(Seq[Point], Seq[Point])] = (points, fill(points.size){ZERO}) #:: {
      for {
        (oldPositions, oldVelocities) <- positionAndVelocity
        newVelocities                 <- calculateVelocity(oldPositions).toSeq
        combinedVelocities            =  oldVelocities.zip(newVelocities).map{case (l, r) => l |+| r}
        combinedPositions             =  oldPositions.zip(combinedVelocities).map{case (l, r) => l |+| r}
      } yield combinedPositions -> combinedVelocities
    }

    positionAndVelocity
  }

  private def lcm(elements: Long*): Long =
    elements
      .map{BigInt(_)}
      .reduceLeft[BigInt]{case (l, r) => (l*r)/l.gcd(r)}
      .toLong

  def part1: LazyList[Long] = for {
    (position, velocity) <- calculatePositionAndVelocity(readPlanets)
    positionEnergy  = position.map{_.toList.map(abs).sum}
    velocityEnergy  = velocity.map{_.toList.map(abs).sum}
  } yield positionEnergy.zip(velocityEnergy).foldLeft{0}{case (acc, (l, r)) => acc + l * r}

  def part2: Seq[Long] = {
    val (position, velocity) = calculatePositionAndVelocity(readPlanets).unzip
    val splitPositions   = position.map{_.unzip3}
    val splitVelocities  = velocity.map{_.unzip3}
    val splitCoordinates = splitPositions.zip(splitVelocities).map{_.zip}.unzip3.toList

    for {
      coordinate  <- splitCoordinates
      repeatIndex <-
        coordinate
          .scanLeft{empty[(Seq[Int], Seq[Int])]}{_ + _}
          .zip(coordinate)
          .takeWhile{case (acc, item) => !acc.contains(item)}
          .lastOption
          .map{case (map, _) => map.size}
    } yield repeatIndex + 1
  }

  println(s"part1: ${part1 drop(1000) head}")
  println(s"part2: ${lcm(part2:_*)}")

}