import Day5._
import LazyList.unfold
import math.signum

object Day13 extends App {
  val EMPTY = -1L

  val BLOCK = 2
  val PADDLE = 3
  val BALL = 4

  private def registries: Registries = readInstructions("Day13.txt")
  private def run(state: State): Iterator[LazyList[(State, Long)]] =  compute{state}.grouped(3)

  def part1: Int = run{State(registries)}.count{case _ :+ (_ -> tile) => tile == BLOCK}

  def part2: LazyList[Long] = unfold{State(registries.updated(0, 2)) -> (0: Long, 0: Long)}{
    case (state, coords@(paddleX, ballX)) => for {
      output           <- run{state}.nextOption
      (states, results) = output.unzip
      nextState        <- states.lastOption
    } yield results match {
      case -1 +: 0 +: score  +: _ => score -> (nextState, coords)
      case px +: _ +: PADDLE +: _ => EMPTY -> (nextState, (px, ballX))
      case bx +: _ +: BALL   +: _ => EMPTY -> (nextState.enqueue{signum(bx - paddleX)}, (paddleX, bx))
      case _ => EMPTY -> (nextState, coords)
    }
  } filterNot{_ == EMPTY}

  println(s"part1: $part1")
  println(s"part2: ${part2.lastOption}")
}