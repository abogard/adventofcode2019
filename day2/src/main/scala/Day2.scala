import scala.language.postfixOps
import scala.io.Source.fromResource
import cats.implicits._

import LazyList.iterate
import scala.util.{Failure, Success, Try}

object Day2 extends App {
  private def readInstructions: Array[Int] =
    fromResource("Day2.txt")
      .getLines
      .mkString
      .split(",")
      .map{_.toInt}

  private def compute(state: Array[Int]): Try[Int] =
    state
      .grouped(4)
      .to(LazyList)
      .collectFirstSomeM{
        case Array(1, a, b, c) => Try{state(c) = state(a) + state(b); None}
        case Array(2, a, b, c) => Try{state(c) = state(a) * state(b); None}
        case Array(99, _*) => Success{state(0) some}
        case _ => Failure{new Exception("Malformed data")}
      }.map{_ get}

  private def computeWithReplaced(first: Int, second: Int): Try[Int] = {
    val state = readInstructions

    state(1) = first
    state(2) = second

    compute(state)
  }

  def part1: Try[Int] = computeWithReplaced(12, 2)
  def part2: LazyList[Int] = for {
    noun   <- iterate(0, 99){_ + 1}
    verb   <- iterate(0, 99){_ + 1}
    result <- computeWithReplaced(noun, verb).toOption if result == 19690720
  } yield 100 * noun + verb

  println(s"part1: $part1")
  println(s"part2: ${part2 headOption}")
}
