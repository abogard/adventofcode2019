import scala.io.Source.fromResource
import scala.language.postfixOps
import scala.language.implicitConversions
import scala.math.pow

import cats.implicits._

import scala.collection.immutable.Queue
import Map.empty

import LazyList.unfold

object Day5 extends App {
  type IC = Long
  type Registries = Map[Long, Long]

  def readInstructions(file: String): Map[Long, Long] =
    fromResource(file)
      .getLines
      .mkString
      .split(",")
      .map{_.toLong}
      .zipWithIndex
      .map{case (v, i) => i.toLong -> v}
      .toMap
      .withDefaultValue(0L)

  private def registries: Registries = readInstructions("Day5.txt")

  sealed trait ArgMode
  case object Immediate extends ArgMode
  case object Registry extends ArgMode
  case object Relative extends ArgMode

  sealed case class Arg(pos: Long, mode: ArgMode, state: State) {
    private def get: Long = state.registries(pos)

    def value: Long = mode match {
      case Relative => get + state.relativeBase
      case _        => get
    }
    def modValue: Long = mode match {
      case Immediate => get
      case Registry => state.registries(get)
      case Relative => state.registries(get + state.relativeBase)
    }
  }

  sealed case class State(
    registries: Registries = empty[Long, Long],
    input: Queue[Long] = Queue(),
    counter: IC = 0L,
    relativeBase: Long = 0) {

    def update(into: Long, value: Long, move: IC = 0L): State =
      copy(registries = registries.updated(into, value), counter = counter + move)

    def enqueue(inputs: Long*): State = copy(input = input.enqueueAll(inputs))
  }

  object I { def unapply(arg: Arg): Option[Long] = arg.value some }
  object P { def unapply(arg: Arg): Option[Long] = arg.modValue some }

  object Inst {
    def getMode(div: Long, exp: Int): ArgMode = (div / pow(10, exp).toInt) % 10 match {
      case 0 => Registry
      case 1 => Immediate
      case 2 => Relative
    }

    def unapply(state: State): Option[(Long, LazyList[Arg])] =
      (state, LazyList.iterate(state.counter){_ + 1}.map{state.registries(_)}) match {
        case (State(_, _, c, _), hd+:tl) => hd % 100 ->
          tl.zipWithIndex
            .map{case (_, i) => Arg(c + i + 1, getMode(hd, i + 2), state)}
            .to(LazyList) some
        case _ => None
      }
  }

  implicit def wrapState(state: State): Option[(Option[(State, Long)], State)] = None -> state some

  def compute(state: State): LazyList[(State, Long)] = unfold{state}{
    case s@State(_, in, co, rb) => s match {
      case Inst(99, _) => None
      case Inst(1, P(a) +: P(b) +: I(c) +: _) => s.update(c, a + b, 4L)
      case Inst(2, P(a) +: P(b) +: I(c) +: _) => s.update(c, a * b, 4L)
      case Inst(3, I(a) +: _) if in.nonEmpty => s.update(a, in.head, 2L).copy(input = in.tail)
      case Inst(4, P(a) +: _) => Option(s.copy(counter = co + 2), a).map{case o@(ns, _) => Option(o) -> ns}
      case Inst(5, P(a) +: P(b) +: _) => s.copy(counter = if (a != 0) b else co + 3)
      case Inst(6, P(a) +: P(b) +: _) => s.copy(counter = if (a == 0) b else co + 3)
      case Inst(7, P(a) +: P(b) +: I(c) +: _) => s.update(c, if (a < b) 1 else 0, 4L)
      case Inst(8, P(a) +: P(b) +: I(c) +: _) => s.update(c, if (a == b) 1 else 0, 4L)
      case Inst(9, P(a) +: _) => s.copy(relativeBase =  rb + a, counter = co + 2)
    }
  } flatten

  println(s"part1: ${compute{State(registries, Queue(1))}.lastOption.map{case (_, o) => o}}")
  println(s"part2: ${compute{State(registries, Queue(5))}.lastOption.map{case (_, o) => o}}")
}