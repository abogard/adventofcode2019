import scala.io.Source.fromResource
import scala.language.postfixOps
import scala.language.implicitConversions
import scala.collection.MultiDict
import scala.math.max

object Day6 extends App {
  val toVertex   = "SAN"
  val fromVertex = "YOU"

  type Visited  = Set[String]
  type VertexId = String
  type Distance = Int

  lazy val paths: MultiDict[VertexId, VertexId] =
    MultiDict from fromResource("Day6.txt")
      .getLines
      .map{_.split("\\)")}
      .collect{case Array(l, r, _*) => (r.trim, l.trim)}

  lazy val biPaths: MultiDict[String, String] = paths concat paths.map(_.swap)

  def part1(orbits: Iterable[VertexId] = paths.keySet toSeq): Int =
    orbits map{paths get _} map{next => next.size + part1(next)} sum

  def part2(c: VertexId = fromVertex, d: Distance = 0, v: Visited = Set(fromVertex)): LazyList[(VertexId, Distance)] =
    (c, d) #:: {
      for {
        adjacent  <- biPaths get c to LazyList if !v.contains(adjacent)
        recursive <- part2(adjacent, d + 1, v + c)
      } yield recursive
    }

  println(s"part1: ${part1()}")
  println(s"part2: ${part2() collectFirst{case (id, d) if id == toVertex => max(d-2, 0)}}")
}