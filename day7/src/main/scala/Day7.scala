import java.net.URI

import scala.language.postfixOps
import scala.language.implicitConversions
import cats.implicits._
import Day5._

import LazyList.unfold
import scala.collection.immutable.{Iterable, Queue}

object Day7 extends App {

  private def registries: Registries = readInstructions("Day7.txt")

  private def computeChain(states: LazyList[State], initInput: Long = 0L): LazyList[(State, Long)] = {
    lazy val computations: LazyList[(State, Long)] =
      states
        .zip{initInput #:: computations.map{case (_, v) => v}}
        .map{case(s@State(_, in, _, _), v) => compute(s.copy(input = in.enqueue(v)))}
        .takeWhile{_ nonEmpty}
        .flatMap{_ headOption}

    computations
  }

  def part1: LazyList[Long] = for {
    perm        <- (0 to 4).permutations to LazyList
    states      = perm map{v => State(registries, Queue(v))} to LazyList
    (_, output) <- computeChain(states) lastOption
  } yield output

  def part2: Seq[Long] = for {
    perm   <- (5 to 9).permutations to LazyList
    states = perm.map{v => State(registries, Queue(v))}
    lastComp <- unfold(states.to(LazyList) -> 0L){
      case (Seq(), _)  => None
      case (states, v) => computeChain(states, v) match {
        case LazyList()          => None
        case next@_ :+(_ -> res) => res -> (next.map{_._1}, res) some
      }
    }
  } yield lastComp

  println(s"part1 ${part1 max}")
  println(s"part2 ${part2 max}")
}