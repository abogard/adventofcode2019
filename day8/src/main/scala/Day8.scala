import scala.io.Source.fromResource
import scala.language.postfixOps
import scala.language.implicitConversions

object Day8 extends App {
  private lazy val paths: Array[Array[Int]] =
    fromResource("Day8.txt")
      .getLines
      .flatMap(_.toCharArray)
      .map{_.toString}
      .map{_.toInt}
      .toArray
      .grouped(25*6)
      .toArray

  private lazy val findLayer: Array[Int] = paths minBy{_ count(_ == 0)}
  private lazy val mergedLayers: Array[Int] = paths.transpose.map{_.collectFirst{case x if x != 2 => x}.get}

  def part1: Int = findLayer.count(_ == 1) * findLayer.count(_ == 2)
  def part2: Unit = for {
    j <- 0 until 6
    i <- 0 until 25
  } {
    if (i == 0) println()
    print(mergedLayers(i + (j * 25)))
  }

  println(s"part1: $part1")
  println("part2: ")
  part2

}