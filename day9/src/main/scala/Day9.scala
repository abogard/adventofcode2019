import Day5._

import scala.collection.immutable.Queue
import scala.language.{implicitConversions, postfixOps}

object Day9 extends App {
  private def registries: Registries = readInstructions("Day9.txt")

  println(s"part1: ${compute{State(registries, Queue(1))}.toList.map{case (_, o) => o}}")
  println(s"part2: ${compute{State(registries, Queue(2))}.toList.map{case (_, o) => o}}")
}

